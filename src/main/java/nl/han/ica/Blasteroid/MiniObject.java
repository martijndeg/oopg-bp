package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;

public class MiniObject extends AutonomousGameObject {

	/**
	 * Constructor for MiniObject
	 * 
	 * @param int randomSpeed
	 * @param int randomDirection
	 * @param GameInfo gameInfo
	 * @param Blasteroid blasteroid
	 */
	public MiniObject(float randomSpeed, int randomDirection, GameInfo gameInfo, Blasteroid blasteroid) {
		this(randomDirection, randomDirection,
				gameInfo, new Sprite("src/main/java/nl/han/ica/Blasteroid/Content/meteorGrey_tiny2.png"), blasteroid);
		this.pointsWorth = 1500;
		this.lifes = 1;
		this.gameInfo = gameInfo;
		this.randomDirection = randomDirection;
		this.randomSpeed = randomSpeed;
		this.objectSize = 4;
		
	}

	/**
	 * Constructor for MiniObject
	 * 
	 * @param int pointsWorth
	 * @param int lives
	 * @param GameInfo gameInfo
	 * @param Sprite sprite
	 * @param Blasteroid blasteroid
	 */
	private MiniObject(int pointsWorth, int lives, GameInfo gameInfo, Sprite sprite, Blasteroid blasteroid) {
		super(pointsWorth, lives, gameInfo, sprite, blasteroid);
	}
}
