package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Dashboard.Dashboard;

public class GameHud extends Dashboard {

	/**
	 * Constructor for GameHud
	 * 
	 * @param float x
	 * @param float y
	 * @param float width
	 * @param float height
	 */
	public GameHud(float x, float y, float width, float height) {
		super(x, y, width, height);
	}
	
}
