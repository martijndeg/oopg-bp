package nl.han.ica.Blasteroid;

import java.util.List;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.GameObject;

public class PlayerBullet extends Bullet {
	
	/**
	 * Constructor for PlayerBullet
	 * 
	 * @param rotationAngle
	 * @param speed
	 * @param blasteroid
	 */
	public PlayerBullet(float rotationAngle, int speed, Blasteroid blasteroid) {
		super(rotationAngle, speed, blasteroid);
	}
	
	@Override
	public void gameObjectCollisionOccurred(List<GameObject> collidedGameObjects) {
		for (GameObject g:collidedGameObjects) {
            if (g instanceof AutonomousGameObject || g instanceof AutonomousEnemy) {
            	blasteroid.deleteGameObject(this);
            }
		}
	}
}
