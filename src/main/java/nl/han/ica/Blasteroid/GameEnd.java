package nl.han.ica.Blasteroid;

import java.util.Timer;
import java.util.TimerTask;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.SpriteObject;

public class GameEnd extends SpriteObject {
	
	private Timer timer = new Timer();
	private GameInfo gameInfo;
	private Blasteroid blasteroid;
	private int seconds = 0;
	
	/**
	 * Timer for counting seconds
	 * 
	 * TODO: refactor (rename?)
	 */
	TimerTask task = new TimerTask() {
		@Override
		public void run() {
			seconds++;
		}
	};
	
	/**
	 * Constructor for GameEnd
	 * 
	 * @param GameInfo gameInfo
	 * @param Blasteroid blasteroid
	 */
	public GameEnd(GameInfo gameInfo, Blasteroid blasteroid) {
		this(new Sprite("src/main/java/nl/han/ica/Blasteroid/Content/GameOverScreen.png"));
		this.gameInfo = gameInfo;
		this.blasteroid = blasteroid;
		timer.scheduleAtFixedRate(task, 1000, 1000);
	}

	/**
	 * Constructor for GameEnd
	 * 
	 * @param Sprite sprite
	 */
	private GameEnd(Sprite sprite) {
		super(sprite);
	}
	
	/**
	 * Restart game 10 seconds after game over
	 */
	private void gameEndTimer() {
		if(seconds == 10) {
			gameInfo.setGameState(1);
			blasteroid.deleteGameObject(this);
			task.cancel();
		}
	}

	@Override
	public void update() {
		gameEndTimer();
	}
}
