package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;

public class MediumSmallEnemy extends AutonomousEnemy {

	/**
	 * Constructor for MediumSmallEnemy
	 * 
	 * @param int randomDirection
	 * @param float randomSpeed
	 * @param Player player
	 * @param GameInfo gameInfo
	 * @param Blasteroid blasteroid
	 */
	public MediumSmallEnemy(int randomDirection, float randomSpeed, Player player, GameInfo gameInfo, Blasteroid blasteroid) {
		this(randomDirection, randomSpeed, randomDirection, randomDirection, player, gameInfo, new Sprite("src/main/java/nl/han/ica/Blasteroid/Content/small_MediumSmall.png"), blasteroid);
		this.randomDirection = randomDirection;
		this.randomSpeed = randomSpeed;
		this.player = player;
		this.blasteroid = blasteroid;
		this.pointsWorth = 4000;
		this.lifes = 1;
		this.gameInfo = gameInfo;
	}
	
	/**
	 * Constructor for MediumSmallEnemy
	 * 
	 * @param int randomDirection
	 * @param float randomSpeed
	 * @param int pointsWorth
	 * @param int lives
	 * @param Player player
	 * @param GameInfo gameInfo
	 * @param Sprite sprite
	 * @param Blasteroid blasteroid
	 */
	private MediumSmallEnemy(int randomDirection, float randomSpeed, int pointsWorth, int lives, Player player, GameInfo gameInfo, Sprite sprite, Blasteroid blasteroid) {
		super(randomDirection, randomSpeed, pointsWorth, lives, player, gameInfo, sprite, blasteroid);
	}
}
