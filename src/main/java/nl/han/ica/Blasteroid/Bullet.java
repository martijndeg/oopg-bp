package nl.han.ica.Blasteroid;

import java.util.List;

import nl.han.ica.OOPDProcessingEngineHAN.Collision.ICollidableWithGameObjects;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.GameObject;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.SpriteObject;
import processing.core.PApplet;
import processing.core.PGraphics;

public class Bullet extends SpriteObject implements ICollidableWithGameObjects {
	
	private float rotationAngle;
	
	protected Blasteroid blasteroid;
	
	/**
	 * Constructor for Bullet
	 * 
	 * @param float rotationAngle
	 * @param int speed
	 * @param Blasteroid blasteroid
	 */
	public Bullet(float rotationAngle, int speed, Blasteroid blasteroid) {
		this(new Sprite("src/main/java/nl/han/ica/Blasteroid/Content/laserBlue02.png"));
		this.rotationAngle = rotationAngle;
		fired(this.rotationAngle, speed);
		this.blasteroid = blasteroid;
	}
	
	/**
	 * Constructor for Bullet
	 * 
	 * @param Sprite sprite
	 */
	private Bullet(Sprite sprite) {
		super(sprite);
	}
	
	@Override
	public void draw(PGraphics g) {
		g.pushMatrix();
		g.translate(getCenterX(), getCenterY());
		g.rotate(PApplet.radians(rotationAngle));
		g.image(getImage(), -width / 2, - height / 2);
		g.popMatrix();
	}
	
	/**
	 * Movement of bullet after firing a bullet
	 * 
	 * @param float rotationAngle
	 * @param int speed
	 */
	private void fired(float rotationAngle, int speed) {
		setDirectionSpeed(rotationAngle, speed);		
	}
	
	/**
	 * Destroy the ArrayList entry of bullet after leaving viewport
	 */
	private void screenEnd() {
		if (getX() < 0 || getX() > 1024 || getY() < 0 || getY() > 720){
			blasteroid.deleteGameObject(this);
		}
	}

	@Override
	public void gameObjectCollisionOccurred(List<GameObject> collidedGameObjects) {
	}

	@Override
	public void update() {
		screenEnd();
	}
}

