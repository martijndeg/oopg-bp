package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;

public class MediumObject extends AutonomousGameObject {

	/**
	 * Constructor for MediumObject
	 * 
	 * @param int randomSpeed
	 * @param float randomDirection
	 * @param GameInfo gameInfo
	 * @param Blasteroid blasteroid
	 */
	public MediumObject(float randomSpeed, int randomDirection, GameInfo gameInfo, Blasteroid blasteroid) {
		this(randomDirection, randomDirection,
				gameInfo, new Sprite("src/main/java/nl/han/ica/Blasteroid/Content/meteorGrey_med2.png"), blasteroid);
		this.pointsWorth = 1000;
		this.lifes = 1;
		this.gameInfo = gameInfo;
		this.randomDirection = randomDirection;
		this.randomSpeed = randomSpeed;
		this.objectSize = 2;
	}

	/**
	 * Constructor for MediumObject
	 * 
	 * @param int pointsWorth
	 * @param int lives
	 * @param GameInfo gameInfo
	 * @param Sprite sprite
	 * @param Blasteroid blasteroid
	 */
	private MediumObject(int pointsWorth, int lives, GameInfo gameInfo, Sprite sprite, Blasteroid blasteroid) {
		super(pointsWorth, lives, gameInfo, sprite, blasteroid);
	}
}
