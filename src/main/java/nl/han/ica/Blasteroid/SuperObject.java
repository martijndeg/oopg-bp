package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;

public class SuperObject extends AutonomousGameObject {

	/**
	 * Constructor for SuperObject
	 * 
	 * @param float randomSpeed
	 * @param int randomDirection
	 * @param GameInfo gameInfo
	 * @param Blasteroid blasteroid
	 */
	public SuperObject(float randomSpeed, int randomDirection, GameInfo gameInfo, Blasteroid blasteroid) {
		this(randomDirection, randomDirection,
				gameInfo, new Sprite("src/main/java/nl/han/ica/Blasteroid/Content/meteorGrey_big4.png"), blasteroid);
		this.pointsWorth = 500;
		this.lifes = 1;
		this.gameInfo = gameInfo;

		this.randomDirection = randomDirection;
		this.randomSpeed = randomSpeed;
		this.objectSize = 0;
	}

	/**
	 * Constructor for SuperObject
	 * 
	 * @param int pointsWorth
	 * @param int lives
	 * @param GameInfo gameInfo
	 * @param Sprite sprite
	 * @param Blasteroid blasteroid
	 */
	private SuperObject(int pointsWorth, int lives, GameInfo gameInfo, Sprite sprite, Blasteroid blasteroid) {
		super(pointsWorth, lives, gameInfo, sprite, blasteroid);
	}
}
