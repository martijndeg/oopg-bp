package nl.han.ica.Blasteroid;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class EnemySpawner {
	
	private Blasteroid blasteroid;
	private Player player;
	private GameInfo gameInfo;
	private Timer timer = new Timer();
	private AutonomousEnemy enemy;
	
	private int worldWidth = 1024;
	private int worldHeight = 720;
	private int seconds = 15;
	private int enemySize;
	private int enemyAmount = 0;


	/**
	 * Timer to add enemies every 20 seconds
	 */
	TimerTask task = new TimerTask() {
		@Override
		public void run() {
			seconds++;
			if(seconds == 20) {
				for(int i = 0; i < enemyAmount; i++) {
					createEnemy();
				}
				seconds = 0;
			}
		}
	};

	/**
	 * Constructor for EnemySpawner
	 * 
	 * @param Blasteroid blasteroid
	 * @param Player player
	 * @param GameInfo gameInfo
	 */
	public EnemySpawner(Blasteroid blasteroid, Player player, GameInfo gameInfo){
		this.blasteroid = blasteroid;
		this. player = player;
		this.gameInfo = gameInfo;
		timer.scheduleAtFixedRate(task, 1000, 1000);
	}
	
	/**
	 * Spawn enemy based on size
	 * 
	 * TODO: Refactor (rename?)
	 * @param int enemySize
	 */
	public void spawnEnemies(int enemySize) {
		this.enemySize = enemySize;
		enemyAmount++;
	}
	
	/**
	 * Place enemy in viewport and start random movement
	 */
	private void createEnemy() {
	Random randomMovement = new Random();
	Random randomPos = new Random();
	float xPosSmall = randomPos.nextInt(worldWidth);
	float yPosSmall = randomPos.nextInt(worldHeight);
	if(gameInfo.getGameState() == 2) {
		switch(enemySize) {
		case 0:
			enemy = new SmallEnemy(randomMovement.nextInt(360), randomMovement.nextFloat() * (1.5f), player, gameInfo, blasteroid);
			break;
		case 1:
			enemy = new MediumSmallEnemy(randomMovement.nextInt(360), randomMovement.nextFloat() * (1.5f), player, gameInfo, blasteroid);
			break;
		case 2:
			enemy = new MediumEnemy(randomMovement.nextInt(360), randomMovement.nextFloat() * (1.5f), player, gameInfo, blasteroid);
			break;
		case 3:
			enemy = new MediumLargeEnemy(randomMovement.nextInt(360), randomMovement.nextFloat() * (1.5f), player, gameInfo, blasteroid);
			break;
		case 4:
			enemy = new LargeEnemy(randomMovement.nextInt(360), randomMovement.nextFloat() * (1.5f), player, gameInfo, blasteroid);
			break;
			}
		}
	blasteroid.addGameObject(enemy, xPosSmall, yPosSmall);
	}
	
	/**
	 * Setter for EnemyAmount
	 * 
	 * @param int enemyAmount
	 */
	public void setEnemyAmount(int enemyAmount) {
		this.enemyAmount = enemyAmount;
	}

	/**
	 * Setter for seconds
	 * 
	 * @param int seconds
	 */
	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}
}
	
