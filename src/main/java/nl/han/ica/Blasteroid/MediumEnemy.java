package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;

public class MediumEnemy extends AutonomousEnemy {
	
	/**
	 * Constructor for MediumEnemy
	 * 
	 * @param int randomDirection
	 * @param float randomSpeed
	 * @param Player player
	 * @param GameInfo gameInfo
	 * @param Blasteroid blasteroid
	 */
	public MediumEnemy(int randomDirection, float randomSpeed, Player player, GameInfo gameInfo, Blasteroid blasteroid) {
		this(randomDirection, randomSpeed, randomDirection, randomDirection, player, gameInfo, new Sprite("src/main/java/nl/han/ica/Blasteroid/Content/mediumVijand.png"), blasteroid);
		this.randomDirection = randomDirection;
		this.randomSpeed = randomSpeed;
		this.player = player;
		this.blasteroid = blasteroid;
		this.pointsWorth = 3000;
		this.lifes = 1;
		this.gameInfo = gameInfo;
	}
	
	/**
	 * Constructor for MediumEnemy
	 * 
	 * @param int randomDirection
	 * @param float randomSpeed
	 * @param int pointsWorth
	 * @param int lives
	 * @param Player player
	 * @param GameInfo gameInfo
	 * @param Sprite sprite
	 * @param Blasteroid blasteroid
	 */
	private MediumEnemy(int randomDirection, float randomSpeed, int pointsWorth, int lives, Player player, GameInfo gameInfo, Sprite sprite, Blasteroid blasteroid) {
		super(randomDirection, randomSpeed, pointsWorth, lives, player, gameInfo, sprite, blasteroid);
	}
}
