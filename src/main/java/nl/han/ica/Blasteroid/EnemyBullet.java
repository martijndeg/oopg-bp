package nl.han.ica.Blasteroid;

import java.util.List;

import nl.han.ica.OOPDProcessingEngineHAN.Collision.ICollidableWithGameObjects;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.GameObject;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.SpriteObject;
import processing.core.PApplet;
import processing.core.PGraphics;

public class EnemyBullet extends Bullet{

	/**
	 * Constructor for EnemyBullet
	 * 
	 * @param float rotationAngle
	 * @param int speed
	 * @param Blasteroid blasteroid
	 */
	public EnemyBullet(float rotationAngle, int speed, Blasteroid blasteroid) {
		super(rotationAngle, speed, blasteroid);
	}
	
	@Override
	public void gameObjectCollisionOccurred(List<GameObject> collidedGameObjects) {
		for (GameObject g:collidedGameObjects) {
            if (g instanceof Player) {
            	blasteroid.deleteGameObject(this);
            }
		}
	}
}
