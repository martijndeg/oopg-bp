package nl.han.ica.Blasteroid;

import java.util.Random;

import nl.han.ica.OOPDProcessingEngineHAN.Engine.GameEngine;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.GameObject;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;
import nl.han.ica.OOPDProcessingEngineHAN.View.View;
import processing.core.PApplet;

@SuppressWarnings("serial")
public class Blasteroid extends GameEngine {

	private Sprite playerSprite = new Sprite(
			"src/main/java/nl/han/ica/Blasteroid/Content/spaceshipPlaceholderKlein.png");
	private TitleScreen titleScreen;
	private GameInfo gameInfo = new GameInfo();
	private DashboardText scoreText = new DashboardText("");
	private DashboardText livesText = new DashboardText("");
	private DashboardText levelText = new DashboardText("");
	private Player player = new Player(1000, 1, gameInfo, playerSprite, this);
	private EnemySpawner enemySpawner = new EnemySpawner(this, player, gameInfo);
	private AsteroidSpawner asteroidSpawner = new AsteroidSpawner(this, gameInfo);
	
	private static int worldWidth = 1024;
	private static int worldHeight = 720;
	
	private GameHud dashboard = new GameHud(0, 0, worldWidth, 100);
	private GameEnd gameEnd;
	private InstructionScreen instructionScreen;

	/**
	 * Main entry point for application
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] { "nl.han.ica.Blasteroid.Blasteroid" });
	}

	@Override
	public void setupGame() {
		createGameScreen(worldWidth, worldHeight);
		gameState();
	}
	
	/**
	 * Building the view based on gameInfo.gameState
	 */
	private void gameState() {
		switch (gameInfo.getGameState()) {
		case 1:
			titleScreen();
			break;
		case 2:
			deleteGameObject(titleScreen);
			createObjects();
			createGameHud(worldWidth, 100);
			break;
		case 3:
			gameEnd();
			dashboard.deleteAllDashboardObjects();
			break;
		case 4:
			instructionScreen();
			break;
		}
	}

	/**
	 * Adding player and asteroid / enemy objects 
	 */
	private void createObjects() {
		levelCreator();
		if (gameInfo.getLevel() < 6) {
			addGameObject(player, 512, 360);
		}
	}
	
	/**
	 * Spawning Asteroids and Enemies based on the current level
	 */
	private void levelCreator() {
		Random random = new Random();
		enemySpawner.setEnemyAmount(0);
		enemySpawner.setSeconds(15);
		if(gameInfo.getLevel() < 5) {
			for(int i = 0; i < gameInfo.getLevel();i++) {
				asteroidSpawner.spawnAsteroids(random.nextInt(5));
				enemySpawner.spawnEnemies(random.nextInt(5));
			}
		}
		else if(gameInfo.getLevel() >= 6) {
			gameInfo.setGameState(3);
			deleteGameObject(player);
			gameState();
		}
	}
	
	/**
	 * Restart game after game over
	 */
	private void restartGame() {
		if (getGameObjectItems().size() == 0) {
			titleScreen();
			gameInfo.setLevel(0);
			gameInfo.setLives(5);
			gameInfo.setScore(0);
		}
	}

	/**
	 * End the game
	 */
	private void gameEnd() {
		int amount = 0;
		for (GameObject g : getGameObjectItems()) {
			if (g instanceof GameEnd) {
				amount++;
			}
		}
		if (amount < 1) {
			deleteAllGameOBjects();
			gameEnd = new GameEnd(gameInfo, this);
			addGameObject(gameEnd, 0, 0);
		}
	}

	@Override
	public void update() {
		scoreText.setText("" + gameInfo.getScore(), 150, 57);
		livesText.setText("" + gameInfo.getLives(), 510, 57);
		levelText.setText("" + gameInfo.getLevel(), 810, 57);
		NextLevel();
		restartGame();
		buttonPressed();
	}
	
	/**
	 * Determine if instructions button is pressed in main menu 
	 */
	private void buttonPressed() {
		int amount = 0;
		for (GameObject g : getGameObjectItems()) {
			if (g instanceof InstructionScreen) {
				amount++;
				System.out.println(amount);
			}
		}
		if (gameInfo.isButtonPressed() && gameInfo.getGameState() != 2) {
			gameState();
			if (gameInfo.getGameState() == 1) {
				if(amount == 1) {
					deleteGameObject(instructionScreen);
					instructionScreen.deleteButtons();
				}
			}
		}
	}
	
	/**
	 * Move to next level 
	 */
	private void NextLevel() {
		int amount = 0;
		int playerAmount = 0;
		int level = gameInfo.getLevel();
		if (gameInfo.getGameState() == 2) {
			titleScreen.deleteButtons();
			for (GameObject g : getGameObjectItems()) {
				if (g instanceof AutonomousGameObject) {
					amount++;
				} else if (g instanceof Player) {
					playerAmount++;
				}
			}
			if (amount == 0) {
				level++;
				gameInfo.setLevel(level);
				gameState();
			} else if (playerAmount == 0) {
				gameInfo.setGameState(3);
				gameState();
			}
		}
	}
	
	/**
	 * Set up viewport
	 * 
	 * @param screenWidth
	 * @param screenHeight
	 */
	private void createGameScreen(int screenWidth, int screenHeight) {
		View view = new View(screenWidth, screenHeight);
		setView(view);
		view.setBackground(loadImage("src/main/java/nl/han/ica/Blasteroid/Content/BG_final.png"));
		size(screenWidth, screenHeight);
	}

	/**
	 * Display instructions screen
	 */
	private void instructionScreen() {
		int amount = 0;
		for (GameObject g : getGameObjectItems()) {
			if (g instanceof InstructionScreen) {
				amount++;
			}
		}
		if (amount < 1) {
			instructionScreen =  new InstructionScreen(this, gameInfo);
			addGameObject(instructionScreen);
			instructionScreen.createButtons();
		}
	}

	/**
	 * Display title screen (main menu)
	 */
	private void titleScreen() {
		int amount = 0;
		for (GameObject g : getGameObjectItems()) {
			if (g instanceof TitleScreen) {
				amount++;
			}
		}
		if (amount < 1) {
			titleScreen = new TitleScreen(this, gameInfo);
			addGameObject(titleScreen, 0, 0);
			titleScreen.createButtons();
		}
	}

	/**
	 * Create the HUD while game is active
	 * 
	 * @param dashboardWidth
	 * @param dashboardHeight
	 */
	private void createGameHud(int dashboardWidth, int dashboardHeight) {
		if (gameInfo.getLevel() < 6) {
			dashboard.addGameObject(scoreText);
			dashboard.addGameObject(livesText);
			dashboard.addGameObject(levelText);
			addDashboard(dashboard);
		}
	}
}