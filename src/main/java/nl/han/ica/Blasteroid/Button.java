package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.SpriteObject;
import nl.han.ica.OOPDProcessingEngineHAN.UserInput.IMouseInput;


public class Button extends SpriteObject implements IMouseInput {
	
	private GameInfo gameInfo;
	private Blasteroid blasteroid;
	private int gameState;
	
	/**
	 * Constructor for Button
	 * 
	 * @param Sprite sprite
	 * @param Blasteroid blasteroid
	 * @param int gameState
	 * @param GameInfo gameInfo
	 */
	public Button(String sprite, Blasteroid blasteroid, int gameState, GameInfo gameInfo) {
		this(new Sprite(sprite));
		this.blasteroid = blasteroid;
		this.gameInfo = gameInfo;
		this.gameState = gameState;
	}

	/**
	 * Constructor for Button
	 * 
	 * @param Sprite sprite
	 */
	private Button(Sprite sprite) {
		super(sprite);
	}
	
	@Override
	public void update() {
	}
	
	@Override
	public void mousePressed(int x, int y, int button) {
		if(x > getX() && x < getX() + getWidth() && y > getY() && y < getY() + getHeight()) {
			gameInfo.setGameState(gameState);
			gameInfo.setButtonPressed(true);
		}
	}
	
	@Override
	public void mouseReleased(int x, int y, int button) {
		if(x > getX() && x < getX() + getWidth() && y > getY() && y < getY() + getHeight()) {
			System.out.println("this fires");
			gameInfo.setButtonPressed(false);
		}
	}
}
