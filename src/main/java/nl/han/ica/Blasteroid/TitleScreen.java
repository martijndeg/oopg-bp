package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.SpriteObject;

public class TitleScreen extends SpriteObject {
	private Blasteroid blasteroid;
	private Button startButton;
	private Button instructionButton;
	private GameInfo gameInfo;
	
	/**
	 * Constructor for TitleScreen
	 * 
	 * @param Blasteroid blasteroid
	 * @param GameInfo gameInfo
	 */
	public TitleScreen(Blasteroid blasteroid, GameInfo gameInfo){
		this(new Sprite("src/main/java/nl/han/ica/Blasteroid/Content/TitlescreenBG.png"));
		this.blasteroid = blasteroid;
		this.gameInfo = gameInfo;
	}
	
	/**
	 * Constructor for TitleScreen
	 * 
	 * @param Sprite sprite
	 */
	private TitleScreen(Sprite sprite) {
		super(sprite);
	}
	
	/**
	 * Create buttons for TitleScreen
	 */
	public void createButtons() {
		startButton = new Button("src/main/java/nl/han/ica/Blasteroid/Content/StartKnop.png", blasteroid, 2, gameInfo);
		blasteroid.addGameObject(startButton,341,500);
		instructionButton = new Button("src/main/java/nl/han/ica/Blasteroid/Content/instructionsKnop.png", blasteroid, 4, gameInfo);
		blasteroid.addGameObject(instructionButton,300,600);
	}
	
	public void deleteButtons() {
		blasteroid.deleteGameObject(startButton);
		blasteroid.deleteGameObject(instructionButton);
	}

	@Override
	public void update() {		
	}
}
