package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;

public abstract class AttackingGameObject extends DestroyableGameObject {

	/**
	 * Constructor for AttackingGameObject
	 * 
	 * @param int pointsWorth
	 * @param int lifes
	 * @param Sprite sprite
	 */
	public AttackingGameObject(int pointsWorth, int lifes, Sprite sprite) {
		super(pointsWorth, lifes, sprite);
	}
	
	/**
	 * Abstract method to shoot weapon
	 */
	abstract void shoot();
	
}
