package nl.han.ica.Blasteroid;

public class Weapon  {
	
	private AttackingGameObject attackingGameObject;
	private float rotationAngle;
	private int speed;
	
	protected PlayerBullet[] bullets = new PlayerBullet[4];
	protected Blasteroid blasteroid;
	protected int bulletSlots;
	
	/**
	 * Constructor for Weapon
	 * 
	 * @param bulletSlots
	 * @param rotationAngle
	 * @param speed
	 * @param attackingGameObject
	 * @param blasteroid
	 */
	public Weapon(int bulletSlots, float rotationAngle, int speed, AttackingGameObject attackingGameObject, Blasteroid blasteroid) {
		this.blasteroid = blasteroid;
		this.rotationAngle = rotationAngle;
		this.speed = speed;
		this.bulletSlots = bulletSlots;
		this.attackingGameObject = attackingGameObject;
		loadBullets();
	}
	
	/**
	 * Loading bullets into bullets[] array
	 */
	private void loadBullets() {
		for (int i = 0; i < bullets.length; i++) {
			bullets[i] = new PlayerBullet(rotationAngle, speed, blasteroid);
		}
	}
			
	/**
	 * Abstract method for shooting a weapon
	 * 
	 * @param float direction
	 * @param int speed
	 * @param float xPos
	 * @param float yPos
	 */
	public void shoot(float xPos, float yPos) {
		if(attackingGameObject instanceof Player) {
			for(int i = 0; i < bulletSlots; i++) {
				xPos = xPos + (i * 10);
				blasteroid.addGameObject(bullets[i],xPos,yPos);
			}
		}
		if(attackingGameObject instanceof AutonomousEnemy) {
			EnemyBullet enemyBullet = new EnemyBullet(rotationAngle, speed, blasteroid);
			blasteroid.addGameObject(enemyBullet, xPos, yPos);
		}
	}
}