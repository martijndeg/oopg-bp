package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;

public class LargeObject extends AutonomousGameObject {


	/**
	 * Constructor for LargeObject
	 * 
	 * @param float randomSpeed
	 * @param int randomDirection
	 * @param GameInfo gameInfo
	 * @param Blasteroid blasteroid
	 */
	public LargeObject(float randomSpeed, int randomDirection, GameInfo gameInfo, Blasteroid blasteroid) {
		this(randomDirection, randomDirection,
				gameInfo, new Sprite("src/main/java/nl/han/ica/Blasteroid/Content/meteorGrey_big2.png"), blasteroid);
		this.pointsWorth = 750;
		this.lifes = 1;
		this.gameInfo = gameInfo;
		this.randomDirection = randomDirection;
		this.randomSpeed = randomSpeed;
		this.objectSize = 1;
	}

	/**
	 * Constructor for LargeObject
	 * 
	 * @param int pointsWorth
	 * @param int lives
	 * @param GameInfo gameInfo
	 * @param Sprite sprite
	 * @param Blasteroid blasteroid
	 */
	private LargeObject(int pointsWorth, int lives, GameInfo gameInfo, Sprite sprite, Blasteroid blasteroid) {
		super(pointsWorth, lives, gameInfo, sprite, blasteroid);
	}
}
