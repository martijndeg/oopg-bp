package nl.han.ica.Blasteroid;

public class GameInfo {

	private int gameState = 1;
	private int lives;
	private int level;
	private int score;
	private int activeGun = 0;
	private boolean buttonPressed = false;
	
	/**
	 * Setter for buttonPressed
	 * 
	 * @param boolean buttonPressed
	 */
	public void setButtonPressed(boolean buttonPressed) {
		this.buttonPressed = buttonPressed;
	}

	
	/**
	 * Constructor for GameInfo
	 */
	GameInfo() {
		this.lives = 5;
		this.level = 0;
		this.score = 0;
	}

	
	/**
	 * Constructor for GameInfo
	 * 
	 * @param int lives
	 * @param int level
	 * @param int score
	 */
	GameInfo(int lives, int level, int score) {
		this.lives = lives;
		this.level = level;
		this.score = score;
	}
	
	/**
	 * Check if instruction button is pressed
	 * 
	 * TODO: refactor (rename?)
	 * 
	 * @return boolean
	 */
	public boolean isButtonPressed() {
		return buttonPressed;
	}

	
	/**
	 * Setter for lives
	 * 
	 * @param int lives
	 */
	public void setLives(int lives) {
		this.lives = lives;
	}

	/**
	 * Getter for gameState
	 * 
	 * @return int
	 */
	public int getGameState() {
		return gameState;
	}

	/**
	 * Setter for gameState
	 * 
	 * @param int gameState
	 */
	public void setGameState(int gameState) {
		this.gameState = gameState;
	}
	
	/**
	 * Getter for lives
	 * 
	 * @return int
	 */
	public int getLives() {
		return lives;
	}
	
	/**
	 * Gaining or loosing a life
	 * 
	 * @param boolean gain
	 */
	public void changeLives(boolean gain) {
		if(gain == true) {
			this.lives++;
		} else {
			this.lives--;
		}
	}
	
	/**
	 * Getter for level
	 * 
	 * @return int
	 */
	public int getLevel() {
		return level;
	}
	
	/**
	 * Setter for level
	 * 
	 * @param int level
	 */
	public void setLevel(int level) {
		this.level = level;
	}
	
	/**
	 * Getter for score
	 * 
	 * @return int
	 */
	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}


	/**
	 * Add to score or substract from score
	 * 
	 * @param int delta
	 */
	public void changeScore(int delta) {
		if(score + delta < 0) {
			this.changeLives(false);
			this.score = 0;
		} else {
			this.score = score + delta;
		}
	}
	/**
	 * Getter for activeGun
	 * 
	 * @return int
	 */
	public int getActiveGun() {
		return activeGun;
	}

	/**
	 * Setter for activeGun
	 * 
	 * @param int activeGun
	 */
	public void setActiveGun(int activeGun) {
		this.activeGun = activeGun;
	}
}
