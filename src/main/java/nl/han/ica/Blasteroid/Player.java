package nl.han.ica.Blasteroid;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.GameObject;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;
import processing.core.PApplet;
import processing.core.PGraphics;

public class Player extends AttackingGameObject {

	private Blasteroid blasteroid;
	private float rotationAngle = 0;
	private Weapon weapon;
	private int weaponIndex;
	private GameInfo gameInfo;
	private int seconds = 0;
	private Timer timer = new Timer();
	private boolean[] keysPressed = {false, false, false, false};
	
	/**
	 * Timer for duration of weapon upgrade
	 */
	TimerTask task = new TimerTask() {
		@Override
		public void run() {
			seconds++;
		}
	};

	/**
	 * Constructor for Player
	 * 
	 * @param int pointsWorth
	 * @param int lives
	 * @param GameInfo gameInfo
	 * @param Sprite sprite
	 * @param Blasteroid blasteroid
	 */
	public Player(int pointsWorth, int lives, GameInfo gameInfo, Sprite sprite, Blasteroid blasteroid) {
		super(pointsWorth, lives, sprite);
		this.blasteroid = blasteroid;
		this.gameInfo = gameInfo;
		setFriction(0.05f);
		setHeight(40);
		setWidth(40);
		timer.scheduleAtFixedRate(task, 1000, 1000);
	}

	@Override
	public void keyPressed(int keyCode, char key) {
		
		if (keyCode == blasteroid.LEFT) {
			this.keysPressed[0] = true;
		}
		if (keyCode == blasteroid.UP) {
			this.keysPressed[1] = true;
		}
		if (keyCode == blasteroid.RIGHT) {
			this.keysPressed[2] = true;
		}
		if (keyCode == blasteroid.DOWN) {
			this.keysPressed[3] = true;
		}		
		
		setMovement();
		
		if (key == ' ') {
			shoot();
		}
	}
	
	@Override
	public void keyReleased(int keyCode, char key) {
		
		if (keyCode == blasteroid.LEFT) {
			this.keysPressed[0] = false;
		}
		if (keyCode == blasteroid.UP) {
			this.keysPressed[1] = false;
		}
		if (keyCode == blasteroid.RIGHT) {
			this.keysPressed[2] = false;
		}
		if (keyCode == blasteroid.DOWN) {
			this.keysPressed[3] = false;
		}
		
		setMovement();
		
	}
	
	/**
	 * Set the movement for the Object based on which directional key is pressed
	 */
	private void setMovement() {
		final int speed = 5;
		final int rotation = 10;
		
		if (keysPressed[0] == true) {
			rotationAngle -= rotation;
		}
		if (keysPressed[1] == true) {
			setDirectionSpeed(rotationAngle, speed);
		}
		if (keysPressed[2] == true) {
			rotationAngle += rotation;
		}
		if (keysPressed[3] == true) {
			setDirectionSpeed(rotationAngle - 180, speed);
		}
	}

	@Override
	public void draw(PGraphics g) {
		g.pushMatrix();
		g.translate(getCenterX(), getCenterY());
		g.rotate(PApplet.radians(rotationAngle));
		g.image(getImage(), -width / 2, -height / 2);
		g.popMatrix();
	}

	/**
	 * Move the Object to the opposite side of the viewport if it moves over the edge of the viewport 
	 */
	private void screenEnd() {
		if (getX() < 0){
			setX(1024 - (getWidth() + 50));	
		}
		else if (getX() + getWidth() > 1024) {
			setX(0 + 10);
		}
		else if(getY() < 0) {
			setY(720 - (getWidth() + 20));
		}
		else if(getY() + getHeight() > 720) {
			setY(0 + 10);
		}
	}

	@Override
	public void gameObjectCollisionOccurred(List<GameObject> collidedGameObjects) {
		for (GameObject g:collidedGameObjects) {
            if (g instanceof AutonomousGameObject) {            	
            	if(((AutonomousGameObject) g).lastCollision < 0 || ((AutonomousGameObject) g).lastCollision > 3) {
            		hit();
            	} 
            } 
            else if(g instanceof EnemyBullet) {
            	hit();
            }
            else if(g instanceof PowerUp) {
            	seconds = 0;
            }
		}
	}

	@Override
	void destroy() {
		if(gameInfo.getLives() < 0) {
			 blasteroid.deleteGameObject(this);
		}
	}

	@Override
	void hit() {
		gameInfo.changeLives(false);
	}

	@Override
	public void update() {
		destroy();
		screenEnd();
		chooseWeapon();
		weaponIndex = gameInfo.getActiveGun();
		powerUpDuration();
	}
	
	/**
	 * Which weapon the object uses
	 */
	private void chooseWeapon() {
		switch(weaponIndex) {
		case(0):
			weapon = new Weapon(1, rotationAngle, 10, this, blasteroid);
			break;
		case(1):
			weapon = new Weapon(2, rotationAngle, 10, this, blasteroid);
			break;
		case(2):
			weapon = new Weapon(3, rotationAngle, 10, this, blasteroid);
			break;
		case(3):
			weapon = new Weapon(4, rotationAngle, 10, this, blasteroid);
			break;
		}
	}
	
	/**
	 * Duration per weapon upgrade
	 */
	private void powerUpDuration() {
		switch(gameInfo.getActiveGun()) {
		case 0:
			break;
		case 1:
			if(seconds >= 30) {
				gameInfo.setActiveGun(0);
				seconds = 0;
			}
			break;
		case 2:
			if(seconds >= 20) {
				gameInfo.setActiveGun(0);
				seconds = 0;
			}
			break;
		case 3:
			if(seconds >= 10) {
				gameInfo.setActiveGun(0);
				seconds = 0;
			}
			break;
		case 4:
			if(seconds >= 15) {
				gameInfo.setActiveGun(0);
				seconds = 0;
			}
			break;
		}
	}

	@Override
	void shoot() {
		weapon.shoot(getX(), getY());
	}
}