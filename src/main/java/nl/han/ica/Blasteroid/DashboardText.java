package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.GameObject;
import processing.core.PGraphics;

public class DashboardText extends GameObject {
	
	private String text;
	private int xPos;
	private int yPos;
	
	/**
	 * Constructor for TextLevel
	 * 
	 * @param String text
	 */
	public DashboardText(String text) {
		this.text = text;
	}
	
	/**
	 * Setter for text
	 * 
	 * @param String text
	 */
	public void setText(String text, int xPos, int yPos) {
		this.text = text;
		this.xPos = xPos;
		this.yPos = yPos;
	}
	
	@Override
	public void update() {		
	}

	@Override
	public void draw(PGraphics g) {
		g.textAlign(g.TOP);
		g.textSize(30);
		g.text(text, xPos, yPos);
		g.color(255, 255, 255);
	}
}
