package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.SpriteObject;

public class InstructionScreen extends SpriteObject {
	
	private String buttonSprite = "src/main/java/nl/han/ica/Blasteroid/Content/backButton.png";
	private Blasteroid blasteroid;
	private GameInfo gameInfo;
	private Button backButton;

	/**
	 * Constructor for InstructionScreen
	 * 
	 * @param Blasteroid blasteroid
	 * @param GameInfo gameInfo
	 */
	public InstructionScreen(Blasteroid blasteroid, GameInfo gameInfo) {
		this(new Sprite("src/main/java/nl/han/ica/Blasteroid/Content/instructionscreen.png"));
		this.blasteroid = blasteroid;
		this.gameInfo = gameInfo;
	}
	
	
	/**
	 * Constructor for InstructionScreen
	 * 
	 * @param Sprite sprite
	 */
	private InstructionScreen(Sprite sprite) {
		super(sprite);
	}

	@Override
	public void update() {		
	}
	
	/**
	 * Create buttons in view
	 */
	public void createButtons() {
		backButton = new Button(buttonSprite, blasteroid, 1, gameInfo);
		blasteroid.addGameObject(backButton, 400, 600);
	}
	
	/**
	 * Remove buttons in view
	 */
	public void deleteButtons() {
		blasteroid.deleteGameObject(backButton);
	}
}
