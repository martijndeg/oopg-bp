package nl.han.ica.Blasteroid;
import nl.han.ica.OOPDProcessingEngineHAN.Collision.ICollidableWithGameObjects;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.SpriteObject;

public abstract class DestroyableGameObject extends SpriteObject implements ICollidableWithGameObjects {

	protected int pointsWorth;
	protected int lifes;
	
	/**
	 * Constructor for DestroyableGameObject
	 * 
	 * @param int pointsWorth
	 * @param int lives
	 * @param Sprite sprite
	 */
	public DestroyableGameObject(int pointsWorth, int lives, Sprite sprite) {
		super(sprite);
		this.pointsWorth = pointsWorth;
		this.lifes = lives;
	}
	
	/**
	 * Abstract method for destroying the object
	 */
	abstract void destroy();
	
	/**
	 * Abstract method for the object being hit
	 */
	abstract void hit();
}
