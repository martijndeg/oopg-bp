package nl.han.ica.Blasteroid;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.GameObject;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;
import processing.core.PApplet;
import processing.core.PGraphics;

public class AutonomousEnemy extends AttackingGameObject {
	
	private Weapon weapon;
	private PowerUp powerUp;
	private Timer timer = new Timer();
	private int seconds = 0;
	private boolean alive = true;
	
	protected Blasteroid blasteroid;
	protected GameInfo gameInfo;
	protected Player player;
	protected int randomDirection;
	protected float randomSpeed;
		
	/**
	 * TimerTask to shoot at player
	 */
	TimerTask task = new TimerTask() {
		@Override
		public void run() {
			seconds++;
			shoot();
		}
	};
	
	/**
	 * Constructor for AutonomousEnemy
	 * 
	 * @param int randomDirection
	 * @param float randomSpeed
	 * @param int pointsWorth
	 * @param int lives
	 * @param Player player
	 * @param GameInfo gameInfo
	 * @param Sprite sprite
	 * @param Blasteroid blasteroid
	 */
	public AutonomousEnemy(int randomDirection, float randomSpeed, int pointsWorth, int lives, Player player, GameInfo gameInfo, Sprite sprite, Blasteroid blasteroid) {
		super(pointsWorth, lives, sprite);
		this.blasteroid = blasteroid;
		this.randomDirection = randomDirection;
		this.randomSpeed = randomSpeed;
		this.player = player;
		this.gameInfo = gameInfo;
		timer.scheduleAtFixedRate(task, 1000, 1000);
	}
	
	/**
	 * Set the direction of movement 
	 */
	private void direction() {
		setDirectionSpeed(randomDirection, randomSpeed);
	}
	
	/**
	 * Move the Object to the opposite side of the viewport if it moves over the edge of the viewport 
	 */
	private void screenEdge() {
		if (getX() < 0){
			setX(1024 - (getWidth() + 50));	
		}
		else if (getX() + getWidth() > 1024) {
			setX(0 + 10);
		}
		else if(getY() < 0) {
			setY(720 - (getWidth() + 20));
		}
		else if(getY() + getHeight() > 720) {
			setY(0 + 10);
		}
	}
	
	@Override
	public void draw(PGraphics g) {
		g.pushMatrix();
		g.translate(getCenterX(), getCenterY());
		g.rotate(PApplet.radians(randomDirection));
		g.image(getImage(), -width / 2, -height / 2);
		g.popMatrix();
	}

	
	@Override
	public void gameObjectCollisionOccurred(List<GameObject> collidedGameObjects) {
		for (GameObject g:collidedGameObjects) {
            if (g instanceof PlayerBullet) {
            	blasteroid.deleteGameObject(this);
            	alive = false;
            	powerUp = new PowerUp(blasteroid, gameInfo);
            	blasteroid.addGameObject(powerUp, getX(), getY());
            }
		}
	}

	@Override
	void shoot() {
		if(alive) {
			if(gameInfo.getGameState() == 2) {
				if(seconds == 5) {
					weapon.shoot(getX(), getY());
					seconds = 0;
				}
			}
		}
	}
	

	@Override
	void destroy() {
	}

	@Override
	void hit() {
	}

	@Override
	public void update() {
		direction();
		screenEdge();
		weapon = new Weapon(1, getAngleFrom(player) ,10, this, blasteroid);
	}
}
