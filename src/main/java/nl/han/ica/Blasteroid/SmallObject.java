package nl.han.ica.Blasteroid;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;

public class SmallObject extends AutonomousGameObject {

	/**
	 * Constructor for SmallObject
	 * 
	 * @param float randomSpeed
	 * @param int randomDirection
	 * @param GameInfo gameInfo
	 * @param Blasteroid blasteroid
	 */
	public SmallObject(float randomSpeed, int randomDirection, GameInfo gameInfo, Blasteroid blasteroid) {
		this(randomDirection, randomDirection,
				gameInfo, new Sprite("src/main/java/nl/han/ica/Blasteroid/Content/meteorGrey_small2.png"), blasteroid);
		this.pointsWorth = 1250;
		this.lifes = 1;
		this.gameInfo = gameInfo;
		this.randomDirection = randomDirection;
		this.randomSpeed = randomSpeed;
		this.objectSize = 3;
	}

	/**
	 * Constructor for SmallObject
	 * 
	 * @param int pointsWorth
	 * @param int lives
	 * @param GameInfo gameInfo
	 * @param Sprite sprite
	 * @param Blasteroid blasteroid
	 */
	private SmallObject(int pointsWorth, int lives, GameInfo gameInfo, Sprite sprite, Blasteroid blasteroid) {
		super(pointsWorth, lives, gameInfo, sprite, blasteroid);
	}
}
