package nl.han.ica.Blasteroid;

import java.util.List;
import java.util.Random;

import nl.han.ica.OOPDProcessingEngineHAN.Collision.ICollidableWithGameObjects;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.GameObject;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.SpriteObject;

public class PowerUp extends SpriteObject implements ICollidableWithGameObjects {
	
	private Random random = new Random();
	private Blasteroid blasteroid;
	private GameInfo gameInfo;

	/**
	 * Constructor for PowerUp
	 * 
	 * @param Blasteroid blasteroid
	 * @param GameInfo gameInfo
	 */
	public PowerUp(Blasteroid blasteroid, GameInfo gameInfo) {
		this(new Sprite("src/main/java/nl/han/ica/Blasteroid/Content/bolt_gold.png"));
		this.blasteroid = blasteroid;
		this.gameInfo = gameInfo;
	}
	
	/**
	 * Constructor for PowerUp
	 * 
	 * @param Sprite sprite
	 */
	private PowerUp(Sprite sprite) {
		super(sprite);
	}

	@Override
	public void gameObjectCollisionOccurred(List<GameObject> collidedGameObjects) {
		for (GameObject g:collidedGameObjects) {
            if (g instanceof Player) {
            	blasteroid.deleteGameObject(this);
            	gameInfo.setActiveGun(random.nextInt(4) + 1);
            }
		}
	}

	@Override
	public void update() {
	}

}
