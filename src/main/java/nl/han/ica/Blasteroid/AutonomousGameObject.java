package nl.han.ica.Blasteroid;

import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import nl.han.ica.OOPDProcessingEngineHAN.Objects.GameObject;
import nl.han.ica.OOPDProcessingEngineHAN.Objects.Sprite;

public class AutonomousGameObject extends DestroyableGameObject {

	private final Blasteroid blasteroid;
	private Timer collisionTimer = new Timer();
	private AutonomousGameObject splitAsteroidOne;
	private AutonomousGameObject splitAsteroidTwo;
	
	protected GameInfo gameInfo;
	protected int objectSize;
	protected int randomDirection;
	protected float randomSpeed;
	
	public int lastCollision; // Counts seconds since last collision
	
	
	/**
	 * Start timer after collision to prevent instant game over on colliding into a large object
	 * 
	 * @return TimerTask
	 */
	private TimerTask createCollisionTask() {
		return new TimerTask() {
			@Override
			public void run() {
				lastCollision++;
			}
		};
	}
	
	/**
	 * Constructor for AutonomousGameObject
	 * 
	 * @param int pointsWorth
	 * @param int lifes
	 * @param GameInfo gameInfo
	 * @param Sprite sprite
	 * @param Blasteroid blasteroid
	 */
	public AutonomousGameObject(int pointsWorth, int lives, GameInfo gameInfo, Sprite sprite, Blasteroid blasteroid) {
		super(pointsWorth, lives, sprite);
		this.blasteroid = blasteroid;
		this.lastCollision = -1;
	}
	
	@Override
	public void update() {
		direction();
		screenEdge();
	}

	/**
	 * Set the direction and speed of the object
	 */
	private void direction() {
		setDirectionSpeed(randomDirection, randomSpeed);
	}

	/**
	 * Move the Object to the opposite side of the viewport if it moves over the edge of the viewport 
	 */
	private void screenEdge() {
		if (getX() < 0){
			setX(1024 - getWidth());	
		}
		else if (getX() + getWidth() > 1024) {
			setX(0);
		}
		else if(getY() < 0) {
			setY(720 - getHeight());
		}
		else if(getY() + getHeight() > 720) {
			setY(0);
		}
	}

	@Override
	public void gameObjectCollisionOccurred(List<GameObject> collidedGameObjects) {
		for (GameObject g:collidedGameObjects) {
            if (g instanceof PlayerBullet) {
            	blasteroid.deleteGameObject(this);
            	gameInfo.changeScore(this.pointsWorth);
            	split();
            } else if (g instanceof Player) {
            	collisionTimer.scheduleAtFixedRate(createCollisionTask(), 0, 5000);
            	if(lastCollision < 0 || lastCollision > 3) {
            		blasteroid.deleteGameObject(this);
                	gameInfo.changeScore(this.pointsWorth);
                	split();
            	}
            }
		}
	} 
	
	/**
	 * Splitting up an asteroidObject into smaller asteroidObjects after collision with player bullet
	 */
	private void split() {
		Random randomMovement = new Random();
		switch(objectSize) {
		case 0: 
			splitAsteroidOne = new LargeObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			splitAsteroidTwo = new LargeObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			break;
		case 1:
			splitAsteroidOne = new MediumObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			splitAsteroidTwo = new MediumObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			break;
		case 2: 
			splitAsteroidOne = new SmallObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			splitAsteroidTwo = new SmallObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			break;
		case 3:
			splitAsteroidOne = new MiniObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			splitAsteroidTwo = new MiniObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			break;
		}
		
		if(objectSize <= 3) {
			blasteroid.addGameObject(splitAsteroidOne, getX(), getY());
			blasteroid.addGameObject(splitAsteroidTwo, getX(), getY());
		}
	}

	@Override
	void destroy() {
	}

	@Override
	void hit() {
	}
}
