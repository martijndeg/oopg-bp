package nl.han.ica.Blasteroid;

import java.util.Random;

public class AsteroidSpawner {
	
	private Blasteroid blasteroid;
	private GameInfo gameInfo;
	private int worldWidth = 1024;
	private int worldHeight = 720;
	private AutonomousGameObject asteroid;
	
	/**
	 * Constructor for AsteroidSpawner
	 * 
	 * @param Blasteroid blasteroid
	 * @param GameInfo gameInfo
	 */
	public AsteroidSpawner(Blasteroid blasteroid, GameInfo gameInfo) {
		this.blasteroid = blasteroid;
		this.gameInfo = gameInfo;
	}
	
	/**
	 * Adds Asteroids to Blasteroid
	 * 
	 * @param int asteroidSize
	 */
	public void spawnAsteroids(int asteroidSize) {
		Random randomMovement = new Random();
		Random randomPos = new Random();
		float xPos = randomPos.nextInt(worldWidth);
		float yPos = randomPos.nextInt(worldHeight);
		switch(asteroidSize) {
		case 0:;
			asteroid = new MiniObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			break;
		case 1:
			asteroid = new SmallObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			break;
		case 2:
			asteroid = new MediumObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			break;
		case 3:
			asteroid = new LargeObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			break;
		case 4:
			asteroid = new SuperObject(randomMovement.nextFloat() * (0.5f), randomMovement.nextInt(360), gameInfo, blasteroid);
			break;
		}
		blasteroid.addGameObject(asteroid, xPos, yPos);
	}
}
