# OOPG-BP

# Asteroid Blasteroid

![Debris field](http://williamwerk.com/art/debrisfieldsm.jpg)

2098 AD - Earth is lost, you are part of a space convoy 
made up of two space arks. It is your convoys task to 
ensure the survival of the human species.

You are tasked with clearing a safe path for the convoy. 
You and the other space defenders each have to clear 
a sector ahead of the passing of the convoy.


Your briefing:

- Use the arrow keys to navigate your sector.
- Fire at all obstacles (asteroids or alien hostiles) with spacebar
- Pickup powerups from hostiles to upgrade your weapon systems
- Clear your sector in order to move on to the next sector

Uses [HAN Game Engine](https://github.com/nickhartjes/OOPD-GameEngine)
